from fbchat import Client, log
from fbchat.models import *
import apiai, codecs, json


class Bot(Client):

    # Code buat konek ke dialogflow
    def apiaiCon(self):
        self.CLIENT_ACCESS_TOKEN = "72916f1f05d0414f99f2ab8aef55939c"
        self.ai = apiai.ApiAI(self.CLIENT_ACCESS_TOKEN)
        self.request = self.ai.text_request()
        self.request.lang = 'de'  # Default : English
        self.request.session_id = "<SESSION ID, UNIQUE FOR EACH USER>"

    def onMessage(self, author_id=None, message_object=None, thread_id=None, thread_type=ThreadType.USER, **kwargs):
        # tandain chat biar ke read
        self.markAsRead(author_id)

        # Print info on console
        log.info("Message {} from {} in {}".format(message_object, thread_id, thread_type))

        # Establish connection
        self.apiaiCon()

        # Message Text
        msgText = message_object.text

        # Request query/reply for the msg received
        self.request.query = msgText

        # response dari json di dialogflow
        response = self.request.getresponse()

        # Convert json object to a list
        reader = codecs.getdecoder("utf-8")
        obj = json.load(response)

        # Get reply from the list
        reply = obj['result']['fulfillment']['speech']

        # Kirim pesan
        if author_id != self.uid:
            self.send(Message(text=reply), thread_id=thread_id, thread_type=thread_type)

        # Tanda pesan uda dikirim
        self.markAsDelivered(author_id, thread_id)


# akun fb
client = Bot("alfred44@yahoo.com", "oraUMUM!!!")

# Listen chat
client.listen()